FROM quay.io/gitpodified-workspace-images/full

# Install the Deta CLI first
RUN mkdir ~/.deta/bin -p && curl --fail --location --progress-bar --output /tmp/deta-cli.zip \
       https://github.com/deta/deta-cli/releases/download/v1.3.1-beta/deta-x86_64-linux.zip \
    && unzip -o "/tmp/deta-cli.zip" -d ~/.deta/bin \
    && chmod +x ~/.deta/bin/deta \
    && echo 'export PATH=/home/gitpod/.deta/bin:$PATH' >> ~/.zshrc && echo 'export PATH=/home/gitpod/.deta/bin:$PATH' >> ~/.bashrc \
    && rm /tmp/deta-cli.zip || true

# Then install the Doppler CLI
RUN sudo apt-get update && sudo apt-get install -y apt-transport-https ca-certificates curl gnupg \
    && curl -sLf --retry 3 --tlsv1.2 --proto "=https" 'https://packages.doppler.com/public/cli/gpg.DE2A7741A397C129.key' | sudo apt-key add - \
    && echo "deb https://packages.doppler.com/public/cli/deb/debian any-version main" | sudo tee /etc/apt/sources.list.d/doppler-cli.list \
    && sudo install-packages doppler

# The install Httpie for debugging HTTP requests to server.
RUN brew install httpie