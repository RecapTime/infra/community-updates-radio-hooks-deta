const Sentry = require("@sentry/node");
const Tracing = require("@sentry/tracing");
const express = require("express");
const app = express();
const { envVars, result } = require("./lib/env");

Sentry.init({
  dsn: envVars.SENTRY_DSN,
  integrations: [
    // enable HTTP calls tracing
    new Sentry.Integrations.Http({ tracing: true }),
    // enable Express.js middleware tracing
    new Tracing.Integrations.Express({ app }),
  ],

  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 0.8,
});
// RequestHandler creates a separate execution context using domains, so that every
// transaction/span/breadcrumb is attached to its own Hub instance
app.use(Sentry.Handlers.requestHandler());
// TracingHandler creates a trace for every incoming request
app.use(Sentry.Handlers.tracingHandler());

const apiClient = require("./lib/api");

app.get("/", (req, res) => res.send("Hello World!"));

// Main hook for the newsletter
app.post("/hooks/newsletter", async function (req, res, next) {
  const apiRequest = req.body;
  try {
    const discord = apiClient.discordPush({
      sourceName: "Community Radar Newsletter",
      articleTitle: apiRequest.title,
      permalink: apiRequest.permalink,
    });
    const guild = apiClient.guildedPush({
      sourceName: "Community Radar Newsletter",
      articleTitle: apiRequest.title,
      permalink: apiRequest.permalink,
    });
    const tg = await apiRequest.tgPush({
      sourceName: "Community Radar Newsletter",
      articleTitle: apiRequest.title,
      permalink: apiRequest.permalink,
    });
    res.json({
      ok: true,
      reason: "new newsletter issue notification sent",
      results: {
        telegram: {
          statusCode: tg.status,
          result: tg.data,
        },
        guilded: {
          statusCode: guild.status,
          result: guild.data,
        },
        discord: {
          statusCode: discord.status,
          result: discord.data,
        },
      },
    });
  } catch (err) {
    return next(err);
  }
});

// Usually used in tests
app.get("/api/sendTestMessage", async function (req, res, next) {
  try {
    const guild = await apiClient.guildedPush({
      sourceName: "Test broadcast",
      articleTitle: "We're testing the web hook server a bit, please ignore.",
      permalink: "https://lttstore.com",
    });
    const tg = await apiClient.tgPush({
      sourceName: "Test broadcast",
      articleTitle: "We're testing the web hook server a bit, please ignore.",
      permalink: "https://lttstore.com",
    });
    res.json({
      ok: true,
      reason: "Test message successfully sent",
      results: {
        telegram: {
          statusCode: tg.status,
          result: tg.data,
        },
        guilded: {
          statusCode: guild.status,
          result: guild.data,
        },
        discord: {
          statusCode: discord.status,
          result: discord.data,
        },
      },
    });
  } catch (err) {
    return next(err);
  }
});

app.get("/api/debug", (req, res) => {
  if (!envVars.DETA_RUNTIME || envVars.NODE_ENV !== "production") {
    res.json({
      ok: true,
      dumps: {
        dotenv: result.parsed,
      },
    });
  } else {
    res.status(404).json({
      ok: false,
      dumps: {},
    });
  }
});

// The error handler must be before any other error middleware and after all controllers
app.use(Sentry.Handlers.errorHandler());

app.use(function onError(err, req, res, next) {
  // The error id is attached to `res.sentry` to be returned
  // and optionally displayed to the user for support.
  res.statusCode = 500;
  res.header("X-Sentry-Id", res.sentry);
  res.json({
    ok: false,
    reason:
      "Something may go wrong on backend side of things. Our devs are being notified through Sentry for an bugfix.",
    sentryId: res.sentry,
  });
});

if (!envVars.DETA_RUNTIME) {
  const localport = envVars.PORT || "3000";
  app.listen(localport, () => {
    console.log(
      `Listening on port ${localport} at http://localhost:${localport}`
    );
  });
}

// export 'app'
module.exports = app;
