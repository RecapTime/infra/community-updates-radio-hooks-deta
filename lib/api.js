const { envVars } = require("./env");
const axios = require("axios");
const mxSdk = require("matrix-js-sdk");
const debuggerLog = require("./utils");
const { markdownv2: formatMd } = require("telegram-format");
const { html: formatHtml } = require("telegram-format");

// Telegram
const tgBotToken = envVars.TELEGRAM_BOT_TOKEN;
const tgTargetChat = envVars.TELEGRAM_CHAT_ID;

// Discord and Guilded
const ggWebhook = envVars.GUILDED_WEBHOOK_URL;
const dsWebhook = envVars.DISCORD_WEBHOOK_URL;

// Matrix
const matrixHomeserver = envVars.MATRIX_HOMESERVER;
const matrixId = envVars.MATRIX_USER_ID;
const matrixAccessToken = envVars.MATRIX_ACCESS_TOKEN;
const matrixRoomId = envVars.MATRIX_ROOM_ID;
const mxClient = mxSdk.createClient({
  baseUrl: matrixHomeserver,
  userId: matrixId,
  accessToken: matrixAccessToken,
});

/**
 * Author metadata for embeds in Discord and Guilded webhooks
 */
const authorMetadata = {
  name: "Community Updates Radio",
  icon_url:
    "https://mstdn.social/system/accounts/avatars/107/823/913/007/790/865/original/902123059f6982ed.png",
  url: "https://matrix.to/#/#community-updates-radio:envs.net",
};

/**
 * Push an content to Guilded via an webhook in an nicely-formatted embed.
 * @param {object} data - Feed object to push into Guilded
 * @param {string} data.sourceName - Source name, usually the name of an publication
 * @param {string} data.articleTitle - Title of content to be pused
 * @param {string} data.permalink - Permalink to an article
 */
function guildedPush(data) {
  var embedData = [
    {
      author: authorMetadata,
      title: data.sourceName,
      url: data.permalink,
      description: data.articleTitle,
    },
  ];
  debuggerLog(embedData);

  const ApiBaseUrl = ggWebhook;
  const result = axios({
    method: "POST",
    url: ApiBaseUrl,
    data: {
      embeds: embedData,
    },
  });
  return result;
}

/**
 * Push an content to Discord via an webhook in an nicely-formatted embed.
 * @param {object} data - Feed object to push into Telegram
 * @param {string} data.sourceName - Source name, usually the name of an publication
 * @param {string} data.articleTitle - Title of content to be pused
 * @param {string} data.permalink - Permalink to an article
 */
function discordPush(data) {
  var embedData = [
    {
      author: authorMetadata,
      title: data.sourceTitle,
      url: data.permalink,
      description: data.articleTitle,
    },
  ];
  debuggerLog(embedData);
  const ApiBaseUrl = dsWebhook;
  const result = axios({
    method: "POST",
    url: ApiBaseUrl,
    data: {
      embeds: embedData,
    },
  });
  return result;
}

/**
 * Push an content to an Matrix room in an nicely-formatted HTML m.room.message event.
 * @param {object} data - Feed object to push into Telegram
 * @param {string} data.sourceName - Source name, usually the name of an publication
 * @param {string} data.articleTitle - Title of content to be pused
 * @param {string} data.permalink - Permalink to an article
 */
function mxPush(data) {
  var content = {
    body: `**From ${data.sourceName}**: ${data.articleTitle}\n\n${data.permalink}`,
    msgtype: "m.text",
    format: "org.matrix.custom.html",
    formatted_body: `<p><strong>From ${data.sourceName}</strong>: ${data.articleTitle}</p>\n\n<p>${data.permalink}</p>`,
  };

  const result = mxClient.sendEvent(
    matrixRoomId,
    "m.room.message",
    content,
    "",
    (err, res) => {
      /** Triggers our tracing service, Sentry, to send reports */
      if (!err) {
        return res;
      } else {
        return err;
      }
    }
  );
  return result;
}

/**
 * Push an content to an Telegram chat in an nicely-formatted Markdown text.
 * @param {object} data - Feed object to push into Telegram
 * @param {string} data.sourceName - Source name, usually the name of an publication
 * @param {string} data.articleTitle - Title of content to be pused
 * @param {string} data.permalink - Permalink to an article
 */
function tgPush(data) {
  const tgApiBase = `https://api.telegram.org/bot${tgBotToken}/sendMessage`;
  const encodedUrl = encodeURI(data.permalink);
  const escapedUrl = formatMd.escape(data.permalink);
  const escapedArticleTitle = formatMd.escape(data.articleTitle);
  const fromSource = formatMd.bold("From " + data.sourceName);
  var message = fromSource + ": " + escapedArticleTitle + "\n\n" + escapedUrl;
  const buttons = {
    inline_keyboard: [
      [
        {
          text: "Share article on Telegram",
          url: "https://t.me/share/url?url=" + encodedUrl,
        },
      ],
      [
        {
          text: "Subscribe to newsletter",
          url: "https://radar.rtapp.tk/subscribe",
        },
      ],
    ],
  };

  const requestStatus = axios({
    method: "POST",
    url: tgApiBase,
    data: {
      text: message,
      chat_id: tgTargetChat,
      parse_mode: "MarkdownV2",
      reply_markup: buttons,
    },
  });
  return requestStatus;
}

function mstdnPush(data) {
  const ApiBase = `https://${envVars.MASTODON_HOMESERVER}/api/v1/statuses`;
  const bearerToken = `bearer ${envVars.MASTODON_ACCESS_TOKEN}`;
  var statusTxt = `From ${data.sourceName}: ${data.articleTitle}\n\n${data.permalink}`;
  const result = axios.post({
    url: ApiBase,
    headers: {
      Authorization: bearerToken,
    },
    data: {
      status: statusTxt,
    },
  });
  return result;
}

module.exports = {
  guildedPush,
  discordPush,
  tgPush,
  mxPush,
  mstdnPush,
};
