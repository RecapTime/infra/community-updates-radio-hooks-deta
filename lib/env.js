/**
 * If the server is ran outside of Doppler context, `dotenv` will take over the importing of secrets to an Node.js process.
 */
const result =
  process.env.DOPPLER_PROJECT === undefined ? require("dotenv").config() : {};
if (result.error) {
  console.warn(result.error);
}

const envVars = process.env;

module.exports = {
  envVars,
  result,
};
