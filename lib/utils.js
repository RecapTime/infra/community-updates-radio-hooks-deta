const { envVars } = require("./env");
const Sentry = require("@sentry/node");

/**
 * An function to add debugging info the console if DEBUG variable is set,
 * otherwise it'll sent to Sentry for dispatch. Usually called in guildedPush and discordPush functions.
 * @param {any} data
 */
function debuggerLog(data) {
  if (envVars.DEBUG !== undefined || envVars.DEBUG !== "") {
    console.debug(data);
  } else {
    Sentry.captureMessage(data);
  }
}

module.exports = debuggerLog;
