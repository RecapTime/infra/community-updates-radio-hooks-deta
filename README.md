# Community Updates Radio Webhooks server on Deta

This repository resides the webhook logic for our Community Updates Radio feed across platforms. We handle most of feed gathering through Pipedream.

## API Docs

These API docs are currently work in progress, please take a look at `index.js` for the deets.

### POST `/hooks/newsletter`

| Parameter Name | Type                 | Description |
| -------------- | -------------------- | ----------- |
| `title`        | String, JSON request |             |
| `permalink`    | String, JSON request |             |

Example request:

```js
// formatted for usage in Piepdream, chnage endpoint URL and X-Api-Key value if self-hosting
return await require("@pipedreamhq/platform").axios(this, {
  method: "POST",
  url: `https://community-updates-radio-hooks.deta.io/hooks/newsletter`,
  data: {
    title: "Community Radar Vol 1 Issue 1",
    peramlink: "https://radar.recaptime.tk/post/...",
  },
  headers: { "X-Api-Key": "prefixKey.randomGibberishTextere" },
});
```
